-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: geek_fitness
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `server_services`
--

DROP TABLE IF EXISTS `server_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `server_services` (
  `id_server_services` int NOT NULL AUTO_INCREMENT,
  `server` int DEFAULT NULL,
  `IP` varchar(45) NOT NULL,
  `IPv4_address` int DEFAULT NULL,
  `subnet_mask` varchar(45) NOT NULL,
  `default_gateway` varchar(45) NOT NULL,
  `DNS_server` varchar(45) NOT NULL,
  PRIMARY KEY (`id_server_services`),
  KEY `ip address_idx` (`IPv4_address`),
  KEY `server name_idx` (`server`),
  CONSTRAINT `ip address 2` FOREIGN KEY (`IPv4_address`) REFERENCES `ip_addresses` (`id`),
  CONSTRAINT `server name` FOREIGN KEY (`server`) REFERENCES `device_name` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `server_services`
--

LOCK TABLES `server_services` WRITE;
/*!40000 ALTER TABLE `server_services` DISABLE KEYS */;
INSERT INTO `server_services` VALUES (1,23,'static',22,'255.255.255.0','192.168.100.254','8.8.8.8'),(2,24,'static',23,'255.255.255.0','192.168.100.254','192.168.100.3'),(3,25,'static',24,'255.255.255.0','192.168.100.254','192.168.100.3');
/*!40000 ALTER TABLE `server_services` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-25 10:12:40
