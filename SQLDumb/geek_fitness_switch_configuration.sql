-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: geek_fitness
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `switch_configuration`
--

DROP TABLE IF EXISTS `switch_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `switch_configuration` (
  `id_switch_configuration` int NOT NULL AUTO_INCREMENT,
  `switch_name` int DEFAULT NULL,
  `switch_port` int DEFAULT NULL,
  `switch_port_mode` varchar(45) NOT NULL,
  `terminal_device` int DEFAULT NULL,
  `terminal_device_MAC_address` varchar(45) NOT NULL,
  PRIMARY KEY (`id_switch_configuration`),
  KEY `switch name_idx` (`switch_name`),
  KEY `terminal device_idx` (`terminal_device`),
  KEY `switch port_idx` (`switch_port`),
  CONSTRAINT `switch name` FOREIGN KEY (`switch_name`) REFERENCES `device_name` (`Id`),
  CONSTRAINT `switch port` FOREIGN KEY (`switch_port`) REFERENCES `switch_port` (`id`),
  CONSTRAINT `terminal device` FOREIGN KEY (`terminal_device`) REFERENCES `device_name` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `switch_configuration`
--

LOCK TABLES `switch_configuration` WRITE;
/*!40000 ALTER TABLE `switch_configuration` DISABLE KEYS */;
INSERT INTO `switch_configuration` VALUES (1,21,1,'Access',23,'0001.C9B8.896B'),(2,21,2,'Access',24,'0001.C9B8.896B'),(3,21,3,'Access',25,'0001.C9B8.896B'),(4,22,8,'Trunk',16,'00E0.A34C.0401'),(5,22,9,'Trunk',17,'00E0.A34C.0402'),(6,22,10,'Trunk',18,'00E0.A34C.0403'),(7,22,11,'Trunk',19,'00E0.A34C.0404'),(8,22,12,'Trunk',20,'00E0.A34C.0405'),(9,26,4,'--',22,'0010.116C.D301'),(10,26,5,'--',21,'000A.F358.5119'),(11,26,6,'--',26,'---');
/*!40000 ALTER TABLE `switch_configuration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-25 10:12:41
