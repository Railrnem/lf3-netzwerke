from typing import Type
import mysql.connector
from flask import Flask, request, render_template, session, request, redirect
from flask_session import Session
from ipaddress import IPv4Network
import ipaddress
import math
import bcrypt 
from flask_mysqldb import MySQL

# Connection-Details
host="localhost"
user="root"
password="contrasenya"
database="geek_fitness"

# Establish db-connection
db=mysql.connector.connect(host=host, user=user, password=password, database=database)

server= Flask(__name__)
print("\nVerbindung hergestellt")


server.config["SESSION_PERMANENT"] = False
server.secret_key = b'_5#y2L"F4Q8z\n\xecgrwagrawr]/'
server.config["SESSION_TYPE"] = "filesystem"

Session(server)

# Homepage

@server.route('/')
def index():
    return render_template('home.html')

#Errorhandler

@server.errorhandler(404)
def page_not_found(e):
    return "Error, try again"

# Login - logout code

@server.route('/login')
def getlogin():
        if session.get('email') is None:
                return render_template('login.html')
        else:
                name= session['firstName']
                return render_template('question.html', name=name)
                         
@server.route('/logout')
def getlogout():
        session.clear()
        return render_template('logout.html')
           
@server.route('/getaccount')
def getregistration():
    return render_template('/getaccount.html')

@server.route('/registration', methods = ['POST'])
def show_registration():
    if request.method == 'POST':

        firstName_input= request.form['firstName']
        lastName_input = request.form['lastName']
        email_input = request.form['email']
        password_input = request.form['password']


        cursor = db.cursor() 
        cursor.execute("""SELECT * FROM registration WHERE email= %s""", (email_input,))
            
        for x in cursor.fetchall(): 
                y = x[3]
                if y == email_input:
                       return render_template('repeat_email.html') 


        bytes = password_input.encode('utf-8')
        salt = bcrypt.gensalt(10)
        hash = bcrypt.hashpw(bytes, salt)
    
        cursor = db.cursor()
        cursor.execute("INSERT INTO registration (firstName, lastName, email, password) VALUES (%s, %s, %s, %s)", (firstName_input, lastName_input, email_input, hash) )
        db.commit()
       
        return render_template('registration.html')

@server.route('/logged', methods = ['GET', 'POST'])
def logged():
    if request.method == 'POST':
        email_input= request.form['email']
        password_input = request.form['password']

        try:
            cursor = db.cursor()
            cursor.execute("""
            SELECT * FROM registration WHERE email= %s""", (email_input,))
            
            y=""
            
            for x in cursor.fetchall(): 
                y = x


            code= y[4].encode('utf-8')
            bytes2= password_input.encode('utf-8')
            
            
            
            if bcrypt.checkpw(bytes2,code):
                session['registration_id']= y[0]
                session['firstName']= y[1]
                session['lastName']= y[2]
                session['email']= y[3]
                session['password']= y[4]
                print("True")

                name=session['firstName']
                return render_template('loginsuccess.html', name=name)
                
            else:return render_template('error.html')
            
        except: return render_template('error.html')

    else: return render_template('error.html')


@server.route('/logout')
def logout():
    session.clear()
    return redirect("/login")


# administration code

@server.route('/administration')
def administration():
        if session.get('email') is None:
            return redirect('/login',code=302)
        else:
                return render_template('administration.html')


@server.route('/devices_information')
def devices_information():
        cursor = db.cursor()
        cursor.execute("""
                SELECT di.id_devices_information, dn.Name, l.Location, ip.ip_address, sp.Name FROM geek_fitness.devices_information AS di
                inner join geek_fitness.device_name AS dn on di.device = dn.Id
                inner join geek_fitness.locations AS l on di.location = l.ID
                inner join geek_fitness.ip_addresses AS ip on di.ip_address = ip.id
                inner join geek_fitness.switch_port AS sp on di.ports = sp.id;
                """)
        result = cursor.fetchall()
        return render_template('devices_information.html', result=result)


@server.route('/show_devices_search', methods= ['POST'])
def show_devices_search():
        search_input= request.form['tsearch']
        cursor = db.cursor()
        cursor.execute("""SELECT di.id_devices_information, dn.Name, l.Location, ip.ip_address, sp.Name FROM geek_fitness.devices_information AS di
                inner join geek_fitness.device_name AS dn on di.device = dn.Id
                inner join geek_fitness.locations AS l on di.location = l.ID
                inner join geek_fitness.ip_addresses AS ip on di.ip_address = ip.id
                inner join geek_fitness.switch_port AS sp on di.ports = sp.id 
                WHERE dn.Name LIKE %s OR l.Location LIKE %s
                OR ip.ip_address LIKE %s OR sp.Name LIKE %s OR etc LIKE %s""", 
        (search_input,search_input,search_input,search_input,search_input,))
        result = cursor.fetchall()
        if not result:
             return render_template ('error_devices.html', result=result)

        return render_template ('devices_search.html', result=result)


@server.route('/switch_configuration')
def switch_configuration():
        cursor = db.cursor()
        cursor.execute("""
                SELECT sc.id_switch_configuration, dn.Name, sp.Name, sc.switch_port_mode, dn2.Name, sc.terminal_device_MAC_address FROM geek_fitness.switch_configuration AS sc
                inner join geek_fitness.device_name AS dn ON sc.switch_name = dn.Id
                inner join geek_fitness.device_name AS dn2 ON sc.terminal_device = dn2.Id
                inner join geek_fitness.switch_port AS sp ON sc.switch_port = sp.id;
                """)
        result = cursor.fetchall()
        return render_template('switch_configuration.html', result=result)



@server.route('/show_switch_search', methods= ['POST'])
def show_switch_search():
        search_input= request.form['tsearch']
        cursor = db.cursor()
        cursor.execute("""SELECT sc.id_switch_configuration, dn.Name, sp.Name, sc.switch_port_mode, dn2.Name, sc.terminal_device_MAC_address FROM geek_fitness.switch_configuration AS sc
                inner join geek_fitness.device_name AS dn ON sc.switch_name = dn.Id
                inner join geek_fitness.device_name AS dn2 ON sc.terminal_device = dn2.Id
                inner join geek_fitness.switch_port AS sp ON sc.switch_port = sp.id
                WHERE dn.Name LIKE %s OR sp.Name LIKE %s
                OR sc.switch_port_mode LIKE %s OR dn2.Name LIKE %s OR sc.terminal_device_MAC_address LIKE %s""", 
        (search_input,search_input,search_input,search_input,search_input,))
        result = cursor.fetchall()
        if not result:
             return render_template ('error_switch.html', result=result)

        return render_template ('switch_search.html', result=result)


@server.route('/ipam')
def ipam():
        cursor = db.cursor()
        cursor.execute("""
                SELECT ipam.id, n.network, n.subnetmask, ipam.VLAN, l.location FROM geek_fitness.ip_address_management AS ipam
                inner join geek_fitness.network AS n ON ipam.network = n.id
                inner join geek_fitness.locations AS l ON ipam.location = l.ID;
                """)
        result = cursor.fetchall()
        return render_template('ipam.html', result=result)


@server.route('/show_ipam_search', methods= ['POST'])
def show_ipam_search():
        search_input= request.form['tsearch']
        cursor = db.cursor()
        cursor.execute("""SELECT ipam.id, n.network, n.subnetmask, ipam.VLAN, l.location FROM geek_fitness.ip_address_management AS ipam
                inner join geek_fitness.network AS n ON ipam.network = n.id
                inner join geek_fitness.locations AS l ON ipam.location = l.ID
                WHERE n.network LIKE %s OR n.subnetmask LIKE %s
                OR ipam.VLAN LIKE %s OR l.location LIKE %s OR etc LIKE %s""", 
        (search_input,search_input,search_input,search_input,search_input,))
        result = cursor.fetchall()
        if not result:
             return render_template ('error_ipam.html', result=result)

        return render_template ('ipam_search.html', result=result)


@server.route('/server_services')
def server_services():
        cursor = db.cursor()
        cursor.execute("""
                SELECT ss.id_server_services, dn.Name, ss.IP, ip.ip_address, ss.subnet_mask, ss.default_gateway, ss.DNS_server FROM geek_fitness.server_services AS ss
                inner join geek_fitness.device_name AS dn ON ss.server = dn.Id
                inner join geek_fitness.ip_addresses AS ip ON ss.IPv4_address = ip.id;
                """)
        result = cursor.fetchall()
        return render_template('server_services.html', result=result)


@server.route('/show_server_search', methods= ['POST'])
def show_server_search():
        search_input= request.form['tsearch']
        cursor = db.cursor()
        cursor.execute("""SELECT ss.id_server_services, dn.Name, ss.IP, ip.ip_address, ss.subnet_mask, ss.default_gateway, ss.DNS_server FROM geek_fitness.server_services AS ss
                inner join geek_fitness.device_name AS dn ON ss.server = dn.Id
                inner join geek_fitness.ip_addresses AS ip ON ss.IPv4_address = ip.id 
                WHERE dn.Name LIKE %s 
                OR ss.IP LIKE %s OR ip.ip_address LIKE %s OR ss.subnet_mask LIKE %s  OR ss.default_gateway LIKE %s 
                OR ss.DNS_server LIKE %s""", (search_input,search_input,search_input,search_input,search_input,search_input,) )
        result = cursor.fetchall()
        if not result:
             return render_template ('error_server.html', result=result)

        return render_template ('server_search.html', result=result)


@server.route('/show_search_button',  methods= ['POST'])
def show_search_button():
        if request.method == 'POST':
                request.form.to_dict(flat=False)
                search= request.form['tsearch']
                cursor = db.cursor()
                cursor.execute("""
                SELECT * FROM %s""", (search,))
                result = cursor.fetchall()
                return render_template ('show_search_button.html', result=result)



# Subnetting tool

@server.route('/subnet_tool')
def subnets_tool():
        if session.get('email') is None:
            return redirect('/login',code=302)
        else:
                return render_template('subnet_tool.html')


@server.route('/ip_address_tool', methods= ['POST'])
def ip_address_tool():
        try:
                search_input= request.form['tsearch']
                IP_Addr = ipaddress.ip_interface(search_input)
                IP_IPAdrr = IP_Addr.ip
                Net_Addr = IP_Addr.network 
               
                #Network Adresse
                net_int = int(IP_IPAdrr) & int(Net_Addr.netmask)
                Network_Adresse = ipaddress.ip_address(net_int)
                
                #Broadcast Adresse
                broad_int = int(IP_Addr) | int(Net_Addr.hostmask)
                Broadcast_Adresse = ipaddress.ip_address(broad_int)

                hosts_input= request.form['tsearch1']
                hosts=int(hosts_input)
                
                numberadresses = Net_Addr.num_addresses
                size_subnet = math.floor(numberadresses / hosts) -2
                subnetmask_length = 32 - (math.ceil(math.log2(numberadresses)))
                
                subnets_network = []
                subnets_broad = []
                x = 0
                while x < hosts:
                        if x == 0:
                                new_network = ipaddress.ip_address(int(IP_IPAdrr) & int(Net_Addr.netmask))
                                new_broad = new_network + size_subnet + 1
                        else:
                                old_broad = new_broad
                                new_network = ipaddress.ip_address(int(old_broad) + 1)
                                new_broad = ipaddress.ip_address(int(old_broad) + size_subnet + 1 )
                        subnets_network.append(new_network)
                        subnets_broad.append(new_broad)
                        x = x + 1

                return render_template('ip_address_tool.html',net_addr_result=Net_Addr, hosts=hosts, size_subnet=size_subnet, Network_Adresse=Network_Adresse, Broadcast_Adresse=Broadcast_Adresse, subnets_network=subnets_network, subnets_broad=subnets_broad)

        except:  return render_template ('error_ip_address.html')


if __name__ == '__main__':
    server.run(debug=True)
    print("schliesse db-verbindung")
    db.close()
    print("Beende mich")
