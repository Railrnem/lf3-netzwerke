-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: geek_fitness
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `devices_information`
--

DROP TABLE IF EXISTS `devices_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devices_information` (
  `id_devices_information` int NOT NULL AUTO_INCREMENT,
  `device` int DEFAULT NULL,
  `location` int DEFAULT NULL,
  `ip_address` int DEFAULT NULL,
  `ports` int DEFAULT NULL,
  `etc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_devices_information`),
  KEY `location_idx` (`location`),
  KEY `device_idx` (`device`),
  KEY `ip address_idx` (`ip_address`),
  KEY `port_idx` (`ports`),
  CONSTRAINT `device` FOREIGN KEY (`device`) REFERENCES `device_name` (`Id`),
  CONSTRAINT `ip address` FOREIGN KEY (`ip_address`) REFERENCES `ip_addresses` (`id`),
  CONSTRAINT `location` FOREIGN KEY (`location`) REFERENCES `locations` (`ID`),
  CONSTRAINT `port` FOREIGN KEY (`ports`) REFERENCES `switch_port` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices_information`
--

LOCK TABLES `devices_information` WRITE;
/*!40000 ALTER TABLE `devices_information` DISABLE KEYS */;
INSERT INTO `devices_information` VALUES (1,1,1,1,7,NULL),(2,2,1,2,13,NULL),(3,3,1,3,1,NULL),(4,4,1,4,14,NULL),(5,5,2,5,1,NULL),(6,6,2,6,2,NULL),(7,7,3,7,1,NULL),(8,8,3,8,2,NULL),(9,9,4,9,1,NULL),(10,10,4,10,2,NULL),(11,11,5,11,1,NULL),(12,12,5,12,2,NULL),(13,13,5,13,13,NULL),(14,14,5,14,13,NULL),(15,15,5,15,13,NULL),(16,16,6,16,8,NULL),(17,17,6,17,9,NULL),(18,18,6,18,10,NULL),(19,19,6,19,11,NULL),(20,20,6,20,12,NULL),(21,21,6,21,5,NULL),(22,22,6,21,4,NULL),(23,23,7,22,1,NULL),(24,24,7,23,2,NULL),(25,25,7,24,3,NULL),(26,26,8,25,6,NULL),(27,27,11,26,3,NULL),(28,28,11,27,15,NULL),(29,29,11,28,16,NULL),(30,30,11,29,17,NULL),(31,31,11,30,18,NULL),(32,32,11,31,19,NULL),(33,33,11,32,20,NULL),(34,34,11,33,14,NULL),(35,35,4,34,3,NULL),(36,36,4,35,13,NULL);
/*!40000 ALTER TABLE `devices_information` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-25 10:12:41
