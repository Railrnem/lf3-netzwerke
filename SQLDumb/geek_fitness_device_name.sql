-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: geek_fitness
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `device_name`
--

DROP TABLE IF EXISTS `device_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `device_name` (
  `Id` int NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_name`
--

LOCK TABLES `device_name` WRITE;
/*!40000 ALTER TABLE `device_name` DISABLE KEYS */;
INSERT INTO `device_name` VALUES (1,'Management-laptop'),(2,'Gast-Laptop01'),(3,'PC-GF01'),(4,'PC-VW01'),(5,'PC-FI01'),(6,'PC-FI02'),(7,'PC-LA01'),(8,'PC-LA02'),(9,'PC-EK01'),(10,'PC-EK02'),(11,'PC-VK01'),(12,'PC-VK02'),(13,'MA-Laptop01'),(14,'MA-Tablet01'),(15,'KD-Smartphone01'),(16,'SW-Verwaltung'),(17,'SW-Finanzen'),(18,'SW-Lager'),(19,'SW-Einkauf'),(20,'SW-Verkauf'),(21,'SW-Server'),(22,'SW-Core'),(23,'DHCP/DNS-Server'),(24,'Web-Server (bs14.local or rather bs14.net)'),(25,'Fileserve (files.local) (only Ping)'),(26,'R-Internet'),(27,'PC-CT01'),(28,'PC-CT02'),(29,'PC-CT03'),(30,'PC-CT04'),(31,'PC-CT05'),(32,'PC-CT06'),(33,'PC-CT07'),(34,'PC-CT08'),(35,'PC-PT PC-EK (Helmut Schön)'),(36,'Tablet TC');
/*!40000 ALTER TABLE `device_name` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-25 10:12:41
